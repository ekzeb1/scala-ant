//https://github.com/xerial/sbt-sonatype#project-rootsonatypesbt

ThisBuild / organization         := "io.gitlab.ekzeb1"
ThisBuild / organizationName     := "ekzeb1"
ThisBuild / organizationHomepage := Some(url("https://gitlab.com/ekzeb1"))

ThisBuild / scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/ekzeb1/scala-ant"),
    "scm:git@gitlab.com:ekzeb1/scala-ant.git"
  )

)

ThisBuild / developers := List(
  Developer("cigaly", "Gabriel Birke", "gb@birke-software.de", url("http://birke-software.de")),
  Developer("giabao", "Bùi Việt Thành", "thanhbv@sandinh.net", url("https://sandinh.com")),
  Developer("ekzeb", "Alex Menshov", "ekzeb@dev.net", url("https://gitlab.com/ekzeb1"))
)

ThisBuild / description := "scala-ant: Ant support from scala 2.13 on jvm > 11"

ThisBuild / licenses := Seq("Apache 2" -> url("https://www.apache.org/licenses/LICENSE-2.0"))

ThisBuild / homepage := Some(url("https://github.com/username/project"))

// Remove all additional repository other than Maven Central from POM
ThisBuild / pomIncludeRepository := { _ => false }

//ThisBuild / isSnapshot := true

ThisBuild / publishTo := {
  val nexus = "https://s01.oss.sonatype.org/"
  if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
  else Some("releases" at nexus + "service/local/staging/deploy/maven2")
}

ThisBuild / publishMavenStyle := true

ThisBuild / versionScheme := Some("early-semver")
